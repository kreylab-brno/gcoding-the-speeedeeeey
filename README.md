# gcoding-the-speeedeeeey

![](img/gcoding-speedy.png)

Experiments with the Trotec Speedy 400 to make it accept GCODE from FreeCAD to avoid using proprietareyy job control/ruby while making it more functional.

The laser attaches an **FTDI USB Serial Device** that linux mounts to `/dev/ttyUSB0` once the USB is connected to it.

```
[  169.325244] ftdi_sio 1-1.1:1.0: device disconnected
[  172.616090] usb 1-1.1: new full-speed USB device number 7 using ehci-platform
[  172.726193] usb 1-1.1: New USB device found, idVendor=0403, idProduct=6001, bcdDevice= 4.00
[  172.726290] usb 1-1.1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[  172.726302] usb 1-1.1: Product: USB <-> Serial
[  172.726311] usb 1-1.1: Manufacturer: FTDI
[  172.730576] ftdi_sio 1-1.1:1.0: FTDI USB Serial Device converter detected
[  172.731624] usb 1-1.1: Detected FT232BM
[  172.733948] usb 1-1.1: FTDI USB Serial Device converter now attached to ttyUSB0
```

This effectively solves the USB enumeration step for us and should now just accept commands from the linux system, next step is to capture the packets from [wireshark](https://www.wireshark.org) on the current windows system that is used to handle laser and then try to replicate them from linux system.

The current goal is to turn the laser ON and OFF by capturing the `Connect` and `Disconnect` signals from JobControl and documenting them.